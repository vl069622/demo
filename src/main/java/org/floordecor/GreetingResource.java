package org.floordecor;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/demo")
public class GreetingResource {

    private GreetingService greetingService;

    @Inject
    public GreetingResource(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    /**
     * demo quarkus constructor dependency injection
     * @return
     */
    @GET
    @Path("/cdi")
    @Produces(MediaType.TEXT_PLAIN)
    public String demoDependencyInjection() {
        return this.greetingService.demoDependencyInjection();
    }

    @GET
    @Path("/greeting")
    @Produces(MediaType.TEXT_PLAIN)
    public String demo() {
        return "Hello Viet Le";
    }
}