package org.floordecor;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GreetingService {
    public String demoDependencyInjection() {
        return "dependency injection demo";
    }
}
